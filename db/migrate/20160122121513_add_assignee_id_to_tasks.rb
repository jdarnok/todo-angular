class AddAssigneeIdToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :assignee_id, :integer
    add_foreign_key :tasks, :users, column: :assignee_id, on_delete: :cascade
  end
end
