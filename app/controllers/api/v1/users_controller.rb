class Api::V1::UsersController < ApiController
  def index
    @users = User.all
    respond_with @users
  end

  def show
  end

  def create
  end

  def edit
  end

  def delete
  end

  def new
  end
end