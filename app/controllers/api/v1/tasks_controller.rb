class Api::V1::TasksController < ApiController
  before_filter :set_task, only: [:show, :update, :delete]

  def index

    @tasks = Task.all.includes(:user)
    respond_with @tasks
  end

  def show
    respond_with @task
  end

  def create
    user = User.find params[:task][:assignee][:id]
    @task = Task.new(task_params.merge(assignee_id: user.id, user_id: current_user.id))

    if @task.save
      respond_with :api, @task, status: 200
    else
      respond_with(message: 'blad')
    end
  end

  def changeStatus
    status = params[:status]
    @task = Task.find(params[:task_id])
    @task.update(status: status)
    render json: {message: 'OK', status: 200}
  end

  def edit
  end

  def delete
  end

  def new
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:name, :description, :user_id, :status, :assignee)
  end

end