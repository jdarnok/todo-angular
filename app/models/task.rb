class Task < ActiveRecord::Base
  belongs_to :user
  belongs_to :assignee, foreign_key: 'assignee_id', class_name: 'User'
  enum status: [ :unactive, :active, :finished ]
end