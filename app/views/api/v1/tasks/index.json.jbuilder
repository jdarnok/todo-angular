json.array! @tasks do |task|
  json.id task.id
  json.user task.user
  json.assignee task.assignee
  json.status task.status
  json.description task.description
  json.name task.name
end
