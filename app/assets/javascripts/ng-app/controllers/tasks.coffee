angular.module('zbieracz').controller 'TasksCtrl', [
  '$scope'
  'Task'
  'Auth'
  '$rootScope'
  '$state'
  '$uibModal'
  ($scope, Task, Auth, $rootScope, $state, $uibModal) ->
    $scope.visible = false
    Auth.currentUser().then ((user) ->
      $scope.visible = true
      $rootScope.$broadcast 'logged', true

      $scope.newTask = ->
        $state.go 'task'
        return
      $scope.seeTask = (task) ->
        modalInstance = $uibModal.open(
          animation: true
          templateUrl: 'shared/_taskModal.html'
          controller: 'TaskModalCtrl'
          resolve: task: ->
            return task
        )
        modalInstance.result.then ((selectedItem) ->
          $scope.selected = selectedItem
          return
        ), ->
          return

      $scope.editTask = (task) ->
        modalInstance = $uibModal.open(
          animation: true
          templateUrl: 'shared/_editTaskModal.html'
          controller: 'EditTaskModalCtrl'
          resolve: task: ->
            return task
        )
        modalInstance.result.then ((selectedItem) ->
          $scope.selected = selectedItem
          return
        ), ->
          return

      $scope.assignees = [
        'Karl'
        'Bruno'
        'Third One'
      ]
      Task.getTasks().then (data) ->
        $scope.tasks = data
# => {id: 1, ect: '...'}
      $scope.$on('bag-one.drop', (e, el) ->
        status = el.context.parentElement.id
        id = el.context.firstElementChild.id
        Task.changeStatus(id, 1) if status == 'active'
        Task.changeStatus(id, 0) if status == 'unactive'
        Task.changeStatus(id, 2) if status == 'finished'


      )
      return
    ), (error) ->
      $scope.visible = false
      # unauthenticated error
      $rootScope.$broadcast 'logged', false
      $state.go 'login'
      return
    return
]
angular.module('zbieracz').controller 'NewTasksCtrl', [
  '$scope'
  'Task'
  'User'
  '$rootScope'
  '$state'
  'Auth'
  ($scope, Task, User, $rootScope, $state, Auth) ->
    $scope.visible = false
    Auth.currentUser().then ((user) ->
      $scope.visible = true
      $rootScope.$broadcast 'logged', true
      $scope.users = User.query()
      # => {id: 1, ect: '...'}
      return
    ), (error) ->
      $scope.visible = false
      # unauthenticated error
      $rootScope.$broadcast 'logged', false
      $state.go 'login'
      return

    $scope.addTask = (newTask, assignee) ->
      task = {}
      task.description = newTask.description
      task.name = newTask.name
      task.assignee = assignee
      Task.addTask(task).then (response) ->
        console.log response
        $scope.task = response
        $state.go 'tasks'
        return
      return
    return
]
