angular.module('zbieracz')
    .controller('AuthCtrl',
         function($scope, Auth, $rootScope, $state) {
             $scope.submit = function (email, password) {
                 var credentials = {
                     email: $scope.email,
                     password: $scope.password,
                 };
                 var config = {
                     headers: {
                         'X-HTTP-Method-Override': 'POST'
                     }
                 };
                 Auth.login(credentials, config).then(function () {
                     // Successfully logged in.
                     // Redo the original request.
                     console.log("zalogowano");
                     $rootScope.$broadcast('logged', true);
                     $state.go('dashboard.one');

                 }).then(function (response) {
                     // Successfully recovered from unauthorized error.
                     // Resolve the original request's promise.
                 }, function (error) {
                     console.log("blad");
                     $rootScope.$broadcast('logged', false);
                     // There was an error logging in.
                     // Reject the original request's promise.
                 });

             }
         }
    );