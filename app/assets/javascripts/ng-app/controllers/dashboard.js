angular.module('zbieracz')
    .controller('DashboardCtrl', function ($scope, Auth, $rootScope,$state) {
        Auth.currentUser().then(function(user) {
            // User was logged in, or Devise returned
            // previously authenticated session.
            $scope.visible = true;
            $rootScope.$broadcast('logged', true);
            // => {id: 1, ect: '...'}
        }, function(error) {
            $scope.visible = false;
            // unauthenticated error
            $rootScope.$broadcast('logged', false);
            $state.go('login')
        });

    });