angular.module('zbieracz').controller 'TaskModalCtrl',
  ($scope, $uibModalInstance, task) ->
    $scope.task = task

    $scope.ok = ->
      $uibModalInstance.close $scope.selected.task
      return

    $scope.cancel = ->
      $uibModalInstance.dismiss 'cancel'
      return

    return
