angular.module('zbieracz').controller 'EditTaskModalCtrl',
  ($scope, $uibModalInstance, task, Task, User, $rootScope, $state, Auth) ->
    $scope.task = task

    $scope.ok = ->
      $uibModalInstance.close $scope.selected.task
      return

    $scope.cancel = ->
      $uibModalInstance.dismiss 'cancel'
      return

    $scope.visible = false
    Auth.currentUser().then ((user) ->
      $scope.visible = true
      $rootScope.$broadcast 'logged', true
      $scope.users = User.query()
      # => {id: 1, ect: '...'}
      return
    ), (error) ->
      $scope.visible = false
      # unauthenticated error
      $rootScope.$broadcast 'logged', false
      $state.go 'login'
      return

    $scope.addTask = (newTask, assignee) ->
      task = {}
      task.description = newTask.description
      task.name = newTask.name
      task.assignee = assignee
      Task.addTask(task).then (response) ->
        console.log response
        $scope.task = response
        $state.go 'tasks'
        return
      return
    return
