angular.module('zbieracz')
    .controller('HomeCtrl', ['$scope', 'Auth', '$rootScope', '$state', function ($scope, Auth, $rootScope,$state) {
        $scope.visible = false;
        Auth.currentUser().then(function(user) {
            // User was logged in, or Devise returned
            // previously authenticated session.
            $scope.visible = true;
            $rootScope.$broadcast('logged', true);
            // => {id: 1, ect: '...'}
        }, function(error) {
            $scope.visible = false;
            // unauthenticated error
            $rootScope.$broadcast('logged', false);
            $state.go('login')
        });



        $scope.things = ['Angular', 'Rails 4.1', 'UI Router', 'Together!!'];
    }]);