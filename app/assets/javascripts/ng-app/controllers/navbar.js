angular.module('zbieracz')
    .controller('NavbarCtrl', function($scope, Auth, $state, $rootScope){

        $scope.doActive = function(){
        };

        $scope.logOut = function() {
            Auth.logout().then(function(oldUser) {
                // alert(oldUser.name + "you're signed out now.");
                $rootScope.$broadcast('logged', false);
            }, function(error) {
                // An error occurred logging out.
            });
        }

        $scope.$on('logged', function(event, response) {
            if (response === true) {
                $scope.authorized = true;
            }
            if (response === false) {
                $scope.authorized = false;
            }
        });
        // Request requires authorization
        // Will cause a `401 Unauthorized` response,
        // that will be recovered by our listener above.
    });