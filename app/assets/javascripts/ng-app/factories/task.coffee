angular.module('zbieracz').factory 'Task', ($q,$http) ->
#    '$resource'
#    ($resource) ->
#    $resource 'api/tasks/:taskID.json'
  URL = 'api/tasks'
  getTasks: ->
    deferred = $q.defer()
    $http.get "#{URL}.json", null
    .success (data, status)->
      deferred.resolve data
    .error (data, status)->
      deferred.reject "Error"
    return deferred.promise

  changeStatus: (id, status) ->
    deferred = $q.defer()
    $http.post "#{URL}/#{id}/changeStatus.json", {status: status}
    .success (data, status)->
      deferred.resolve data
    .error (data, status)->
      deferred.reject "Error"
    return deferred.promise

  addTask: (task) ->
    deferred = $q.defer()
    $http.post "#{URL}.json", {task: task}
    .success (data, status)->
      deferred.resolve data
    .error (data, status)->
      deferred.reject "Error"
    return deferred.promise