angular.module('zbieracz')
    .factory('User', [
    '$resource', function($resource) {
        return $resource('api/users/:userID.json', null, {
            'update': {
                method: 'PUT'
            }
        });
    }
]);